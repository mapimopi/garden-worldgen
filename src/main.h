#include <cmath>
#include <iostream>
#include <vector>

#include "raylib.h"

#include "simplex.h"
#define STB_PERLIN_IMPLEMENTATION 1
#include "stb_perlin.h"

using namespace std;
using uch = unsigned char;

#include "values.h"
#include "math.h"
#include "graphics.h"

struct Blueprint {
    float tiles[mapWidth * mapHeight];
};
