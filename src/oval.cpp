float distance(Oval &o, int x, int y) // 0 - center; 1 - edge; > 1 - outside
{
    float dx = (x - o.x) / o.rx;
    float dy = (y - o.y) / o.ry;
    return sqrt(dx * dx + dy * dy);
}

bool isInside(Oval &o, int x, int y)
{
    return distance(o, x, y) <= 1;
}

float getNoiseAt(int x, int y, int seed)
{
    float freq2 = 5.f;
    float z = 0;

    float r;

    if (useFbmNoise) {
        r = noiseFbmAt(
            x * freq, y * freq, z * freq, lacunarity, gain, octaves, seed);
    } else {
        r = noiseTurbulenceAt(
            x * freq, y * freq, z * freq, lacunarity, gain, octaves, seed);
    }

    float dist = 9999;
    Continent *cont;

    for (Continent &c : b.continents) {
        float d = distance(c.size, x, y);
        if (d < dist) {
            dist = d;
            cont = &c;
        }
    }

    float continentFactor = 0;
    if (isInside(cont->ridge, x, y)) {
        dist = distance(cont->ridge, x, y);
        continentFactor = 2 - dist;
    } else if (isInside(cont->center, x, y)) {
        dist = distance(cont->center, x, y);
        continentFactor = 1 - dist * 0.5;
    } else if (isInside(cont->size, x, y)) {
        dist = distance(cont->size, x, y);
        continentFactor = 1 - dist;
    }

    r *= continentFactor;

    if (r <= 0) {
        r = 0;
    }

    if (r > 1) {
        // TODO: Ideally this should be avoided
        r = 1;
    }

    return r;
}

void generate()
{
    {
        randomSeed(seed);
        b.continents.clear();
        for (int i = 0; i < continents; i++) {
            float cx = mapWidth / 2;
            float cy = mapHeight / 3;
            float crx = random(10, 20);
            float cry = random(10, 20);

            int randomizer = random(2);
            cx += random(
                -continentPadding * randomizer, continentPadding * randomizer);
            if (random(1) == 0)
                cy += random(-continentPadding, continentPadding * 3);

            float rx = cx + random(-10, 10);
            float ry = cy + random(-10, 10);
            float rrx = random(2, 10);
            float rry = random(2, 10);

            float sx = cx + random(-10, 10);
            float sy = cy + random(-10, 10);
            float srx = random(20, 40);
            float sry = random(20, 40);

            b.continents.push_back({
                .center = {cx, cy, crx, cry},
                .ridge = {rx, ry, rrx, rry},
                .size = {sx, sy, srx, sry},
            });
        }
    }

    {
        for (int x = 0; x < mapWidth; x++) {
            for (int y = 0; y < mapHeight; y++) {
                b.tiles[at(x, y)] = getNoiseAt(x, y, seed);
            }
        }
    }

    if (smoothen) { // Smooth tiles up
        float newTiles[mapWidth * mapHeight];
        float sum;
        int count;
        int s = 1;

        for (int x = 0; x < mapWidth; x++) {
            for (int y = 0; y < mapHeight; y++) {
                if (x < s || x >= mapWidth - 1 - s || y < s ||
                    y >= mapHeight - 1 - s) {
                    newTiles[at(x, y)] = 0;
                    continue;
                }

                sum = 0;
                count = 0;
                for (int i = -s; i <= s; i++) {
                    for (int j = -s; j <= s; j++) {
                        if (b.tiles[at(x + i, y + j)] > 0) {
                            sum += b.tiles[at(x + i, y + j)];
                            count++;
                        }
                    }
                }

                if (count < 2) {
                    count++;
                }

                if (count > 0) {
                    newTiles[at(x, y)] = sum / count;
                } else {
                    newTiles[at(x, y)] = 0;
                }
            }
        }

        for (int x = 0; x < mapWidth; x++) {
            for (int y = 0; y < mapHeight; y++) {
                b.tiles[at(x, y)] = newTiles[at(x, y)];
            }
        }
    }
}
