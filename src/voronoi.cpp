constexpr int polyF = 16;
constexpr int polyWidth = mapWidth / polyF;
constexpr int polyHeight = mapHeight / polyF;

struct Poly {
    float x;
    float y;
    float height;
};

constexpr int circleCount = 16;

struct Circle {
    float x;
    float y;
    float r;
};

Poly polygons[polyWidth * polyHeight];
Circle circles[circleCount];

Poly &polyAt(int i, int j)
{
    return polygons[i + j * polyWidth];
}

void createPolygons()
{
    Poly *poly;
    int pad = 8;
    for (int i = 0; i < polyWidth; i++) {
        for (int j = 0; j < polyHeight; j++) {
            poly = &polyAt(i, j);
            poly->x = random(i * polyF + pad, (i + 1) * polyF - pad);
            poly->y = random(j * polyF + pad, (j + 1) * polyF - pad);

            poly->height =
                simplexAt(i * frequency * 4 + seed, j * frequency * 4 + seed);

            // float dx = mapWidth / 2 - poly->x;
            // float dy = mapHeight / 2 - poly->y;
            // poly->height = sqrt(dx * dx + dy * dy) / (mapWidth / 2);
        }
    }

    int pad = 2;
    for (int i = 0; i < polyWidth; i++) {
        for (int j = 0; j < polyHeight; j++) {
            poly = &polyAt(i, j);
            if (i < pad || i >= polyWidth - pad || j < pad ||
                j >= polyHeight - pad) {
                poly->height = 0;
            }
        }
    }
}

void createCircles()
{
    int pad = 32;
    float minCircle = 20;
    float maxCircle = 60;

    for (int i = 0; i < circleCount; i++) {
        circles[i] = {
            (float)random(pad, mapWidth - pad),
            (float)random(pad, mapHeight - pad),
            (float)random(minCircle, maxCircle),
        };
    }
}

float distanceToPoly(Poly *poly, int x, int y)
{
    float dx = poly->x - x;
    float dy = poly->y - y;
    return sqrt(dx * dx + dy * dy) / (polyF * 2);
}

Poly *closestPoly(int x, int y)
{
    float d;
    Poly *poly;
    float dist = 99999;
    Poly *closest;

    int i0 = x / polyF - 1;
    int j0 = y / polyF - 1;
    int i1 = x / polyF + 1;
    int j1 = y / polyF + 1;

    for (int i = i0; i <= i1; i++) {
        for (int j = j0; j <= j1; j++) {
            if (i >= 0 || i < polyWidth || j >= 0 || j < polyHeight) {
                poly = &polyAt(i, j);
                d = distanceToPoly(poly, x, y);
                if (d < dist) {
                    dist = d;
                    closest = poly;
                }
            }
        }
    }

    return closest;
}

float distanceToCircle(Circle *c, int x, int y)
{
    float dx = c->x - x;
    float dy = c->y - y;
    return sqrt(dx * dx + dy * dy) / c->r;
}

Circle *closestCircle(int x, int y)
{

    float dist = 9999;
    Circle *result;

    for (int i = 0; i < circleCount; i++) {
        float dx = circles[i].x - x;
        float dy = circles[i].y - y;
        float d = sqrt(dx * dx + dy * dy);

        if (d < dist) {
            dist = d;
            result = &circles[i];
        }
    }
    return result;
}

float getNoiseAt(int x, int y, int seed)
{
    Circle *c = closestCircle(x, y);
    float circleDistance = 1 - distanceToCircle(c, x, y);

    Poly *poly = closestPoly(x, y);
    float d = distanceToPoly(poly, x, y);
    // d = 1 - d;
    d = poly->height;

    float r;
    r = fbmAt(x * frequency, y * frequency, lacunarity, gain, octaves, seed);

    // r *= circleDistance * 2;
    r *= d;
    // r *= 2;

    if (r <= 0) {
        r = 0;
    }

    return r;
}

void generate()
{
    randomSeed(seed);
    createPolygons();
    createCircles();

    {
        float n;

        float sum = 0;
        int count = 0;
        float mid = 0;
        float min = 100;
        float max = -100;

        for (int x = 0; x < mapWidth; x++) {
            for (int y = 0; y < mapHeight; y++) {
                n = getNoiseAt(x, y, seed);

                { // Distribution debug
                    sum += n;
                    count += 1;
                    if (n < min)
                        min = n;
                    if (n > max)
                        max = n;
                }

                b.tiles[at(x, y)] = n;
            }
        }

        mid = sum / count;
        cout << "Mid: " << mid << " Min: " << min << " Max: " << max << endl;
    }

    if (smoothen) { // Smooth tiles up
        float newTiles[mapWidth * mapHeight];
        float sum;
        int count;
        int s = 2;

        for (int x = 0; x < mapWidth; x++) {
            for (int y = 0; y < mapHeight; y++) {
                if (x < s || x >= mapWidth - 1 - s || y < s ||
                    y >= mapHeight - 1 - s) {
                    newTiles[at(x, y)] = 0;
                    continue;
                }

                sum = 0;
                count = 0;
                for (int i = -s; i <= s; i++) {
                    for (int j = -s; j <= s; j++) {
                        sum += b.tiles[at(x + i, y + j)];
                        count++;
                    }
                }
                newTiles[at(x, y)] = sum / count;
            }
        }

        for (int x = 0; x < mapWidth; x++) {
            for (int y = 0; y < mapHeight; y++) {
                b.tiles[at(x, y)] = newTiles[at(x, y)];
            }
        }
    }
}
