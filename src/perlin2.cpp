struct Point {
    int x;
    int y;
};

struct Tile {
    float elevation;
    float moisture;
};

struct Circle {
    float x; // Center
    float y; // Center
    float r; // Inner radius
    float s; // Soft falloff radius
    float p; // Minkowski power
};

constexpr int circleCount = 30;
Circle circles[circleCount];

constexpr int minEdgeFade = 4;
constexpr float edgeFadeWidth = 8.0;

Tile tiles[mapWidth * mapHeight];
vector<Point> highPoints;
vector<Point> riverPoints;
constexpr float elevationThreshold = 0.75;
constexpr int riverProbability = 25;

float distanceToEdge(int x, int y)
{
    int dx = min(x, mapWidth - x) - minEdgeFade;
    int dy = min(y, mapHeight - y) - minEdgeFade;
    return fclamp(min(dx, dy) / edgeFadeWidth, 0.0, 1.0);
}

float distanceToCircle(Circle *c, int x, int y)
{
    float dx = c->x - x;
    float dy = c->y - y;
    float d = sqrt(dx * dx + dy * dy);

    float sum = 0;
    sum += power(fabs(c->x - x), c->p);
    sum += power(fabs(c->y - y), c->p);
    d = power(sum, 1.0 / c->p);

    d -= c->r;

    if (d < 0) {
        return 0;
    } else {
        return d;
    }
}

float circlesFactor(int x, int y)
{
    float result = 0;

    for (int i = 0; i < circleCount; i++) {
        float d = distanceToCircle(&circles[i], x, y);
        float f = fclamp(1 - d / circles[i].s, 0.0, 1.0);

        if (f > 0) {
            result += f * (1 - result);
        }
    }

    return result;
}

Circle *closestCircle(int x, int y)
{
    float dist = 99999;
    Circle *result;

    for (int i = 0; i < circleCount; i++) {
        float d = distanceToCircle(&circles[i], x, y);
        if (d < dist) {
            dist = d;
            result = &circles[i];
        }
    }

    return result;
}

float getNoiseAt(int x, int y)
{
    float c = circlesFactor(x, y);
    float e = distanceToEdge(x, y);
    float r = ridgeAt(
        x * frequency * 0.75,
        y * frequency * 0.75,
        lacunarity,
        gain,
        2,
        (seed + 1) * 2);
    float n =
        fbmAt(x * frequency, y * frequency, lacunarity, gain, octaves, seed);

    r = fclamp(power(r, ridgePower), 0.0, 1.0);
    if (r < 0.001)
        r = 0;
    if (isnan(r))
        r = 0;

    // Normalize (almost) fbm between 0 and 1
    n = n / 1.5 + 0.5;

    // Increase ridges
    if (n > 0) {
        n = power(n, 2);
    }

    // Flatten the first noise layer
    // if (n * c > -1.1) {
    //     n = 0.3;
    // }

    // Apply ridges
    if (applyRidge) {
        n = n * r * 1.5;

        // r = n * r;
        // n += r * (1 - n) * 0.5;
    }

    // Apply continent circle mask
    n *= c;

    // Apply edge fade mask
    n *= e;

    if (applyNoise) {
        return fclamp(n, 0.0, 1.0);
    } else {
        return c;
    }
}

void startRiverFrom(int x0, int y0)
{
    int x = x0;
    int y = y0;
    float elevation = tiles[at(x, y)].elevation;

    int nextX;
    int nextY;
    float nextElevation;
    float e;

    int length = 0;
    int maxLength = mapWidth * 2;
    vector<Point> points;

    while (elevation >= 0.05) {
        if (x <= 1 || x >= mapWidth - 2 || y <= 1 || y >= mapHeight - 2) {
            break;
        }
        length++;
        if (length > maxLength) {
            // Discard if too long
            return;
        }

        points.push_back({x, y});

        nextElevation = elevation;
        e = tiles[at(x - 1, y)].elevation;
        if (e < nextElevation) {
            nextX = x - 1;
            nextY = y;
            nextElevation = e;
        }
        e = tiles[at(x + 1, y)].elevation;
        if (e < nextElevation) {
            nextX = x + 1;
            nextY = y;
            nextElevation = e;
        }
        e = tiles[at(x, y - 1)].elevation;
        if (e < nextElevation) {
            nextX = x;
            nextY = y - 1;
            nextElevation = e;
        }
        e = tiles[at(x, y + 1)].elevation;
        if (e < nextElevation) {
            nextX = x;
            nextY = y + 1;
            nextElevation = e;
        }

        if (nextElevation == elevation) {
            // Discard if there's no lower point around
            return;
        }

        x = nextX;
        y = nextY;
        elevation = nextElevation;
    }

    for (Point p : points) {
        riverPoints.push_back(p);
    }
}

void generate()
{
    randomSeed(seed);

    { // Place circles
        for (int i = 0; i < circleCount; i++) {
            float x;
            float y;

            // Quadrant possibility:
            // 0, 1, 2    3    4, 5, 6
            // 7               8
            // 9, 10           11, 12
            int quadrant = random(12);
            int top = contPad;
            int left = contPad * 1.6;
            int bottom = mapHeight - contPad;
            int right = mapWidth - contPad * 1.6;
            int midTop = mapHeight / 2 - contPad;
            int midLeft = mapWidth / 2 - contPad * 0.8;
            int midBottom = mapHeight / 2 + contPad;
            int midRight = mapWidth / 2 + contPad * 0.8;

            switch (quadrant) {
            case 0:
            case 1:
            case 2:
                x = random(left, midLeft);
                y = random(top, midTop);
                break;
            case 3:
                x = random(midLeft, midRight);
                y = random(top, midTop);
                break;
            case 4:
            case 5:
            case 6:
                x = random(midRight, right);
                y = random(top, midTop);
                break;
            case 7:
                x = random(left, midLeft);
                y = random(midTop, midBottom);
                break;
            case 8:
                x = random(midRight, right);
                y = random(midTop, midBottom);
                break;
            case 9:
            case 10:
                x = random(left, midLeft);
                y = random(midBottom, bottom);
                break;
            // case 9:
            //     x = random(midLeft, midRight);
            //     y = random(midBottom, bottom);
            //     break;
            case 11:
            case 12:
                x = random(midRight, right);
                y = random(midBottom, bottom);
                break;
            }

            circles[i] = {
                .x = x,
                .y = y,
                .r = (float)random(contPad / 6, contPad / 4),
                .s = (float)random(contPad / 2, contPad),
                .p = (float)(random(1200, 3800) / 1000.0),
            };
        }
    }

    highPoints.clear();
    riverPoints.clear();

    { // Set initial elevation based on noise and circles, and find high points
        for (int x = 0; x < mapWidth; x++) {
            for (int y = 0; y < mapHeight; y++) {
                tiles[at(x, y)].elevation = getNoiseAt(x, y);
                if (tiles[at(x, y)].elevation > elevationThreshold) {
                    highPoints.push_back({x, y});
                }
            }
        }
    }

    if (applyRivers) { // Place rivers
        for (Point &p : highPoints) {
            if (random(100) <= riverProbability) {
                startRiverFrom(p.x, p.y);
            }
        }

        // Kinda erossion
        float af = 0.7;
        float bf = 0.8;
        float cf = 0.9;
        for (Point &p : riverPoints) {
            tiles[at(p.x, p.y)].elevation = 0;
            tiles[at(p.x - 1, p.y)].elevation *= af;
            tiles[at(p.x + 1, p.y)].elevation *= af;
            tiles[at(p.x, p.y - 1)].elevation *= af;
            tiles[at(p.x, p.y + 1)].elevation *= af;
            tiles[at(p.x - 1, p.y - 1)].elevation *= bf;
            tiles[at(p.x + 1, p.y - 1)].elevation *= bf;
            tiles[at(p.x - 1, p.y + 1)].elevation *= bf;
            tiles[at(p.x + 1, p.y + 1)].elevation *= bf;
            tiles[at(p.x - 2, p.y)].elevation *= cf;
            tiles[at(p.x + 2, p.y)].elevation *= cf;
            tiles[at(p.x, p.y - 2)].elevation *= cf;
            tiles[at(p.x, p.y + 2)].elevation *= cf;
        }
    }

    if (smoothen) { // Smooth tiles up
        float newElevation[mapWidth * mapHeight];
        float sum;
        int count;
        int s = 1;

        for (int x = 0; x < mapWidth; x++) {
            for (int y = 0; y < mapHeight; y++) {
                if (x < s || x >= mapWidth - 1 - s || y < s ||
                    y >= mapHeight - 1 - s) {
                    newElevation[at(x, y)] = 0;
                    continue;
                }

                if (tiles[at(x, y)].elevation == 0) {
                    newElevation[at(x, y)] = 0;
                    continue;
                }

                sum = 0;
                count = 0;
                for (int i = -s; i <= s; i++) {
                    for (int j = -s; j <= s; j++) {
                        sum += tiles[at(x + i, y + j)].elevation;
                        count++;
                    }
                }
                newElevation[at(x, y)] = sum / count;
            }
        }

        for (int x = 0; x < mapWidth; x++) {
            for (int y = 0; y < mapHeight; y++) {
                tiles[at(x, y)].elevation = newElevation[at(x, y)];
            }
        }
    }

    {
        for (int x = 0; x < mapWidth; x++) {
            for (int y = 0; y < mapHeight; y++) {
                b.tiles[at(x, y)] = tiles[at(x, y)].elevation;
            }
        }
    }
}
