int clamp(int val, int min, int max)
{
    if (val < min) {
        return min;
    } else if (val > max) {
        return max;
    } else {
        return val;
    }
}
float fclamp(float val, float min, float max)
{
    if (val < min) {
        return min;
    } else if (val > max) {
        return max;
    } else {
        return val;
    }
}
inline void randomSeed(int seed)
{
    srand(seed);
}
inline int random(int max)
{
    return rand() % (max + 1);
}
inline int random(int min, int max)
{
    return min + (rand() % (max - min + 1));
}

inline float power(float x, float y)
{
    return exp(y * log(x));
}

inline int at(int x, int y)
{
    return x + y * mapWidth;
}

inline static float simplexAt(float x, float y, int seed = 0)
{
    return SimplexNoise::noise(x + seed * 0.25, y + seed * 0.5);
}

inline static float perlinAt(float x, float y, int seed = 0)
{
    return stb_perlin_noise3_seed(x, y, 0, 0, 0, 0, seed);
}

inline static float
fbmAt(float x, float y, float lacunarity, float gain, int octaves, int seed = 0)
{
    return stb_perlin_fbm_noise3_seed(x, y, 0, lacunarity, gain, octaves, seed);
}

inline static float ridgeAt(
    float x,
    float y,
    float lacunarity,
    float gain,
    int octaves,
    int seed = 0)
{
    return 1 -
        fabs(stb_perlin_fbm_noise3_seed(
            x, y, 0, lacunarity, gain, octaves, seed));
}
