constexpr int mapWidth = 256;
constexpr int mapHeight = 256;
constexpr float tileSize = 3;

int seed = 0;

float frequency = 0.03f;
float lacunarity = 2.4f;
float gain = 0.5f;
int octaves = 3;

int contPad = 32;
int ridgePower = 3;

bool applyRidge = true;
bool applyNoise = true;
bool applyRivers = false;
bool smoothen = false;
bool grayscale = false;
bool drawText = true;
