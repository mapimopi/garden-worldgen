#include "main.h"

Blueprint b;

#include "perlin2.cpp"

int main()
{
    InitWindow(mapWidth * tileSize, mapHeight * tileSize, "World generator");
    SetTargetFPS(23);

    generate();

    while (!WindowShouldClose()) {
        // Input
        {
            bool shift =
                IsKeyDown(KEY_LEFT_SHIFT) || IsKeyDown(KEY_RIGHT_SHIFT);
            bool alt = IsKeyDown(KEY_LEFT_ALT) || IsKeyDown(KEY_RIGHT_ALT);

            if (IsKeyPressed(KEY_SPACE)) {
                if (shift) {
                    seed = random(0, 1000);
                }
                generate();
            }

            if (IsKeyPressed(KEY_Q)) {
                if (shift) {
                    contPad -= 1;
                } else {
                    contPad += 1;
                }
                generate();
            }
            if (IsKeyPressed(KEY_W)) {
                if (shift) {
                    ridgePower -= 1;
                } else {
                    ridgePower += 1;
                }
                generate();
            }

            if (IsKeyPressed(KEY_Z)) {
                if (shift) {
                    frequency -= 0.01f;
                } else {
                    frequency += 0.01f;
                }
                generate();
            }
            if (IsKeyPressed(KEY_X)) {
                if (shift) {
                    lacunarity -= 0.1f;
                } else {
                    lacunarity += 0.1f;
                }
                generate();
            }
            if (IsKeyPressed(KEY_C)) {
                if (shift) {
                    gain -= 0.1f;
                } else {
                    gain += 0.1f;
                }
                generate();
            }
            if (IsKeyPressed(KEY_V)) {
                if (shift) {
                    octaves -= 1;
                } else {
                    octaves += 1;
                }
                generate();
            }

            if (IsKeyPressed(KEY_O)) {
                smoothen = !smoothen;
                generate();
            }
            if (IsKeyPressed(KEY_T)) {
                applyRivers = !applyRivers;
                generate();
            }
            if (IsKeyPressed(KEY_Y)) {
                applyRidge = !applyRidge;
                generate();
            }
            if (IsKeyPressed(KEY_U)) {
                applyNoise = !applyNoise;
                generate();
            }

            if (IsKeyPressed(KEY_I)) {
                grayscale = !grayscale;
            }
            if (IsKeyPressed(KEY_SLASH)) {
                drawText = !drawText;
            }
        }

        // Draw
        {
            BeginDrawing();
            ClearBackground(BLACK);
            for (int x = 0; x < mapWidth; x++) {
                for (int y = 0; y < mapHeight; y++) {
                    if (grayscale) {
                        DrawRectangle(
                            x * tileSize,
                            y * tileSize,
                            tileSize,
                            tileSize,
                            getGray(b.tiles[at(x, y)]));
                    } else {
                        DrawRectangle(
                            x * tileSize,
                            y * tileSize,
                            tileSize,
                            tileSize,
                            getColor(b.tiles[at(x, y)]));
                    }
                }
            }

            if (drawText) {
                line = 0;
                drawLine(TextFormat("Q Continent padding: %d", contPad));
                drawLine(TextFormat("W Ridge power: %d", ridgePower));
                drawLine(TextFormat("Z Frequency: %f", frequency));
                drawLine(TextFormat("X Lacunarity: %f", lacunarity));
                drawLine(TextFormat("C Gain: %f", gain));
                drawLine(TextFormat("V Octaves: %d", octaves));
                drawLine("T Toggle rivers");
                drawLine("Y Toggle ridges");
                drawLine("U Toggle noise");
                drawLine("O Toggle smoothing");
                drawLine("I Toggle grayscale");

                int px = GetMouseX() / tileSize;
                int py = GetMouseY() / tileSize;
                DrawText(
                    TextFormat(
                        "%dx%d: %f", px, py, b.tiles[at(px, py)]),
                    0,
                    mapWidth * tileSize - 20,
                    20,
                    GREEN);
            }
            EndDrawing();
        }
    }

    CloseWindow();

    return 0;
}
