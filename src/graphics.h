Color getGray(float value)
{
    return {
        (uch)fclamp(value * 255, 0, 255),
        (uch)fclamp(value * 255, 0, 255),
        (uch)fclamp(value * 255, 0, 255),
        255,
    };
}

Color getColor(float value)
{
    if (value < 0.08) {
        return SKYBLUE;
    } else if (value < 0.12) {
        return GOLD;
    } else if (value < 0.5) {
        return GREEN;
    } else if (value < 0.8) {
        return DARKGREEN;
    } else if (value < 0.9) {
        return GRAY;
    } else {
        return DARKGRAY;
    }
}

Color getShadedColor(float value)
{
    Color color;

    if (value < 0.05) {
        return SKYBLUE;
    } else {
        color = GREEN;
    }

    return {
        (uch)fclamp(color.r - 64 + value * 128, 0, 255),
        (uch)fclamp(color.g - 64 + value * 128, 0, 255),
        (uch)fclamp(color.b - 64 + value * 128, 0, 255),
        255,
    };
}


int fontSize = 18;
int line = 0;
void drawLine(const char *str)
{
    DrawText(str, 2, fontSize * line + 2, fontSize, BLACK);
    DrawText(str, 0, fontSize * line++, fontSize, GREEN);
}
