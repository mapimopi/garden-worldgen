float distanceToEdge(int x, int y)
{
    float pad = 16;

    if (x < pad || y < pad || x > mapWidth - pad || y > mapHeight - pad) {
        return 0;
    }

    // float dx0 = fabs(pad - x);
    // float dx1 = fabs(mapWidth / 3 * 2 - x);
    // float dx2 = fabs(mapWidth / 3 - x);
    // float dx3 = fabs(mapWidth - pad - x);
    // float dx = min(dx0, min(dx3, max(dx1, dx2)));
    float dx0 = fabs(pad - x);
    float dx1 = fabs(mapWidth / 2 - x);
    float dx2 = fabs(mapWidth - pad - x);
    float dx = min(dx0, min(dx1, dx2));

    float dy0 = fabs(pad - y);
    float dy1 = fabs(mapHeight - pad - y);
    float dy = min(dy0, dy1);

    float f = 3.5;

    if (dx < dy) {
        return dx / (mapWidth / f);
    } else {
        return dy / (mapHeight / f);
    }
}

float distanceToCircle(int x, int y, int cx, int cy, int cr)
{
    float dx = cx - x;
    float dy = cy - y;
    float d = 1 - sqrt(dx * dx + dy * dy) / cr;
    if (d < 0) {
        return 0;
    } else {
        return d;
    }
}

float distanceToCenter(int x, int y)
{
    float dx = mapWidth / 2 - x;
    float dy = mapHeight / 2 - y;
    float d = 1 - sqrt(dx * dx + dy * dy) / (mapWidth / 2);
    if (d < 0) {
        return 0;
    } else {
        return d;
    }
}

float getNoiseAt(int x, int y, int seed)
{
    float c; // Continent factor
    c = perlinAt(
        x * frequency * contLacunarity, y * frequency * contLacunarity, seed);
    if (c < 0)
        c = 0;
    else
        c = 1;
    c *= distanceToCenter(x, y) * distanceToEdge(x, y);

    float n =
        fbmAt(x * frequency, y * frequency, lacunarity, gain, octaves, seed);
    n *= c;
    return n;
}

void generate()
{
    randomSeed(seed);

    {
        for (int x = 0; x < mapWidth; x++) {
            for (int y = 0; y < mapHeight; y++) {
                b.tiles[at(x, y)] = 0;
            }
        }
        for (int r = 0; r < 360; r++) {
            float angle = r / 180.0 * PI;
            float length = (simplexAt(r * 0.005, seed) + 1.0) * 30;
            float cx = mapWidth / 2;
            float cy = mapHeight / 2;

            for (int l = 0; l < length; l++) {
                int x = floor(cx + cos(angle) * l);
                int y = floor(cy + sin(angle) * l);
                b.tiles[at(x, y)] = 1;
            }
        }
        return;
    }

    { // Generate tiles (or chunk types)
        float n;
        float sum = 0;
        int count = 0;
        float mid = 0;
        float min = 100;
        float max = -100;

        for (int x = 0; x < mapWidth; x++) {
            for (int y = 0; y < mapHeight; y++) {
                n = getNoiseAt(x, y, seed);

                { // Distribution debug
                    sum += n;
                    count += 1;
                    if (n < min)
                        min = n;
                    if (n > max)
                        max = n;
                }

                b.tiles[at(x, y)] = n;
            }
        }

        mid = sum / count;
        cout << "Mid: " << mid << " Min: " << min << " Max: " << max << endl;
    }

    if (smoothen) { // Smooth tiles up
        float newTiles[mapWidth * mapHeight];
        float sum;
        int count;
        int s = 2;

        for (int x = 0; x < mapWidth; x++) {
            for (int y = 0; y < mapHeight; y++) {
                if (x < s || x >= mapWidth - 1 - s || y < s ||
                    y >= mapHeight - 1 - s) {
                    newTiles[at(x, y)] = 0;
                    continue;
                }

                sum = 0;
                count = 0;
                for (int i = -s; i <= s; i++) {
                    for (int j = -s; j <= s; j++) {
                        sum += b.tiles[at(x + i, y + j)];
                        count++;
                    }
                }
                newTiles[at(x, y)] = sum / count;
            }
        }

        for (int x = 0; x < mapWidth; x++) {
            for (int y = 0; y < mapHeight; y++) {
                b.tiles[at(x, y)] = newTiles[at(x, y)];
            }
        }
    }
}
